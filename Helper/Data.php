<?php

namespace Eparts\PrecificacaoAvancada\Helper;

use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\GroupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices\CollectionFactory;
use Eparts\PrecificacaoAvancada\Model\MarkupFactory;
use Eparts\PrecificacaoAvancada\Model\Pricesperregions;
use Eparts\PrecificacaoAvancada\Model\Regions;

class Data
{
    const POLITICA_DEFINIDA = 'POLÍTICAS COMERCIAIS PRE DEFINIDAS';
    const POLITICA_VARIAVEL = 'POLÍTICAS VARIAVEL';

    /**
     * @var Session
     */
    protected $sessionCustomer;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $store;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var MarkupFactory
     */
    protected $markup;

    /**
     * @var Pricesperregions
     */
    protected $pricesPerRegion;

    /**
     * @var Regions
     */
    protected $regions;


    /**
     * Data constructor.
     * @param Session $session
     * @param ProductFactory $productFactory
     * @param GroupFactory $groupFactory
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $collectionFactory
     * @param MarkupFactory $markupFactory
     * @param Pricesperregions $pricesPerRegion
     * @param Regions $regions
     */
    public function __construct(
        Session $session,
        ProductFactory $productFactory,
        GroupFactory $groupFactory,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        MarkupFactory $markupFactory,
        Pricesperregions $pricesPerRegion,
        Regions $regions
    )
    {
        $this->sessionCustomer = $session;
        $this->productFactory = $productFactory;
        $this->groupFactory = $groupFactory;
        $this->store = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->markup = $markupFactory;
        $this->pricesPerRegion = $pricesPerRegion;
        $this->regions = $regions;
    }


    /**
     * Retorna preçco com desconto variável de linha de produto se aplicável
     *
     * @param $idProduct
     * @return false|float
     */
    public function getDiscountVariable($idProduct)
    {
        if (!$this->sessionCustomer->isLoggedIn()) {
            return false;
        }

        $collection = $this->collectionFactory->create()->addFieldToFilter('customer_id', $this->sessionCustomer->getCustomerId());
        $attributeValue = [];

        foreach ($collection as $data) {
            $attributeValue[$data['attribute_name']][] = [
                'value' => $data['attribute_value'],
                'discount' => $data['factor_discount'],
                'website_ids' => $data['website_ids'],
            ];
        }

        if (empty($attributeValue)) {
            return false;
        }

        $product = $this->productFactory->create()->load($idProduct);

        foreach ($attributeValue as $attribute => $values) {
            $valueAttribute = $product->getData($attribute);
            # valida se atributo informado existe
            if (empty($valueAttribute)) {
                continue;
            }

            foreach ($values as $customerPrices) {
                $websiteIds = explode('.', $customerPrices['website_ids']);
                # Valida store_id
                if (!empty($websiteIds[0])) {
                    $currentWebsiteId = $this->store->getStore()->getWebsiteId();
                    if (!in_array($currentWebsiteId, $websiteIds)) {
                        continue;
                    }
                }

                #valida valor do atributo
                if ($customerPrices['value'] == $valueAttribute) {
                    $fatorDesconto = $customerPrices['discount'];
                    $discount = $fatorDesconto / 100;
                    $totalDiscount = $product->getPrice() * $discount;
                    return floatval($product->getPrice() - $totalDiscount);


                }
            }
        }

        return false;

    }


    /**
     * @param $sku
     * @return false
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getPriceMarkup($sku, $group = false)
    {
        $websiteId = $this->store->getStore()->getWebsiteId();
        $customerGroup = $this->sessionCustomer->getCustomerGroupId();
        if ($group) {
            $customerGroup = $group;
        }
        $collectionMarkup = $this->markup->create()->getCollection()
            ->addFieldToFilter('sku', $sku);

        foreach ($collectionMarkup as $markup) {
            $markupGroupId = $markup->getCustomerGroupId();
            $markupWebsite = $markup->getWebsiteId();

            #valida grupo de usuario
            if ($markupGroupId && $markupGroupId != $customerGroup) {
                continue;
            }

            #valida website id
            if ($markupWebsite && $markupWebsite != $websiteId) {
                continue;
            }

            return $markup->getPrice();
        }

        return false;
    }

    /**
     * @param $regionId
     * @return false|string
     * @throws NoSuchEntityException
     */
    public function getCustomerByRegion($regionId)
    {
        $websiteId = $this->store->getStore()->getWebsiteId();
        $collection = $this->pricesPerRegion->getCollection()
            ->addFieldToFilter('region_id', $regionId);

        if (!$collection->count()) {
            return false;
        }

        foreach ($collection as $price) {
            if ($websiteId == $price->getWebsiteId() || empty($price->getWebsiteId())) {
                return $price->getCustomerGroupId();
            }
        }

        return false;
    }

    /**
     * @param $regionId
     * @return false|string
     * @throws NoSuchEntityException
     */
    public function getCustomerGroupCheckout($regionId)
    {
        $collection = $this->regions->getCollection()
            ->addFieldToFilter('region_id', $regionId);

        if (!$collection->count()) {
            return false;
        }

        foreach ($collection as $regions) {
            $id = $regions->getId();
            $group = $this->getCustomerByRegion($id);
            if ($group) {
                return $group;
            }
        }
    }

    public function getPriceDefault()
    {
        $websiteId = $this->store->getStore()->getWebsiteId();

        $collection = $this->pricesPerRegion
            ->getCollection()
            ->addFieldToFilter('region_id', 0);

        if (!$collection->count()) {
            return false;
        }

        foreach ($collection as $prices) {
            if ($websiteId == $prices->getWebsiteId() || empty($prices->getWebsiteId())) {
                return $prices->getCustomerGroupId();
            }
        }

        return false;
    }

    /**
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function updatePrices($group = false)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->create('Magento\Checkout\Model\Session')->getQuote();

        $items = $quote->getAllItems();
        if ($items) {
            foreach ($items as $item) {
                $markupPrice = $this->getPriceMarkup($item->getSku(), $group);

                if ($markupPrice) {
                    $finalPrice = $markupPrice;
                } else {
                    $finalPrice = $this->getDiscountVariable($item->getProductId());
                }

                if (!$finalPrice) {
                    $finalPrice = $item->getProduct()->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount();
                }

                $item->setCustomPrice($finalPrice);
                $item->setOriginalCustomPrice($finalPrice);
                $item->getProduct()->setIsSuperMode(true);
            }

            $quote->collectTotals();
            $quote->save();
        }
    }
}
