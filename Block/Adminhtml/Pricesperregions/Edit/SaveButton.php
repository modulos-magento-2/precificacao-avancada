<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Pricesperregions\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Eparts\PrecificacaoAvancada\Block\Adminhtml\GenericButton;

/**
 * Class SaveButton
 * @package Eparts\PrecificacaoAvancada\Block\Adminhtml\Pricesperregions\Edit
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData(): array
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
                'url' => $this->getSaveUrl(),
            ],
            'sort_order' => 90,
        ];
    }

    /**
     * @return string
     */
    public function getSaveUrl(): string
    {
        return $this->getUrl('*/pricesperregions/save');
    }
}
