<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Prices\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {

        parent::__construct($context, $registry, $formFactory, $data);
    }


    /**
     * @return \Magento\Backend\Block\Widget\Form\Generic
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $form = $this->_formFactory->create(['data' => [
                'id' => 'edit_form',
                'enctype' => 'multipart/form-data',
                'action' => $this->getData('action'),
                'method' => 'post'
            ]
            ]
        );

        $form->setHtmlIdPrefix('prices_');

        $fieldset = $form->addFieldset(
            'base_fieldset',[]
        );


        $fieldset->addField('attribute_name', 'text', [
                'name' => 'attribute_name',
                'label' => __('Nome Atributo'),
                'id' => 'attribute_name',
                'title' => __('Nome Atributo'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );

        $fieldset->addField('attribute_value', 'text', [
                'name' => 'attribute_value',
                'label' => __('Valor Atributo'),
                'id' => 'attribute_value',
                'title' => __('Valor Attributo'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );

        $fieldset->addField('factor_discount', 'text', [
                'name' => 'factor_discount',
                'label' => __('Fator Desconto (%)'),
                'id' => 'factor_discount',
                'title' => __('Fator Desconto (%)'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );

        $fieldset->addField('website_ids', 'text', [
                'name' => 'website_ids',
                'label' => __('Lojas separado por . (para todos informar 0)'),
                'id' => 'website_ids',
                'title' => __('Lojas separado por . para todos informar 0)'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );

        $fieldset->addField('customer_id', 'hidden', [
                'name' => 'customer_id',
                'id' => 'customer_id',
                'required' => true,
                'value' => $id
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
