<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Prices\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class AddButton extends GenericButton implements ButtonProviderInterface
{
    protected $request;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->request = $request;
        parent::__construct($context, $registry);
    }

    /**
     * @return array
     */
    public function getButtonData(): array
    {
        return [
            'label' => __('Cadastrar Preço'),
            'class' => 'action-secondary',
            'on_click' => sprintf("location.href = '%s';", $this->getUrlAdd()),
            'sort_order' => 2,
        ];
    }

    /**
     * @return string
     */
    public function getUrlAdd()
    {
        $id = $this->request->getParam('id');
        return $this->getUrl('prices/*/add/id/'.$id);
    }
}

