<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class Renderer extends AbstractRenderer
{
    protected $_objectmanager;

    protected $url;

    public function __construct(\Magento\Backend\Model\UrlInterface $backendUrl)
    {
        $this->url = $backendUrl;
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        $id = $row->getData('id');

        $url = $this->url->getUrl('prices/*/delete', ['id' => $id]);
        $url = sprintf("location.href = '%s';", $url);

        return '<a onclick="' . $url . '">Excluir</a>';
    }
}
