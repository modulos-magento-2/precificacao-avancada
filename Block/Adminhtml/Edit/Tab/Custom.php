<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

class Custom extends \Magento\Backend\Block\Template implements TabInterface
{

    protected $_coreRegistry;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    public function getTabLabel()
    {
        return __('Precificação Avançada por Produto');
    }

    public function getTabTitle()
    {
        return __('Precificação Avançada por Produto');
    }

    public function canShowTab()
    {
        return true;
    }

    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    public function isHidden()
    {
        return false;
    }

    public function getTabClass()
    {
        return '';
    }

    public function getTabUrl()
    {
        return $this->getUrl('prices/*/custom', ['_current' => true]);
    }

    public function isAjaxLoaded()
    {
        return true;
    }
}
