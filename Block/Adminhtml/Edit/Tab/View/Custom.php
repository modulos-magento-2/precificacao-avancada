<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Edit\Tab\View;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\Registry;
use Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices\CollectionFactory;

class Custom extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;

    protected $_collectionFactory;


    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        Registry $coreRegistry,
        array $data = []
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('view_custom_grid');
        $this->setSortable(false);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('customer_id', $this->_request->getParam('id'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }


    protected function _prepareColumns()
    {
        $this->addColumn(
            'attribute_name',
            [
                'header' => __('Atributo'),
                'index' => 'attribute_name',
            ]
        );

        $this->addColumn(
            'attribute_value',
            [
                'header' => __('Valor'),
                'index' => 'attribute_value',
            ]
        );

        $this->addColumn(
            'factor_discount',
            [
                'header' => __('Fator Desconto'),
                'index' => 'factor_discount',
            ]
        );


        $this->addColumn(
            'website_ids',
            [
                'header' => __('Website Ids'),
                'index' => 'website_ids',
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Remover Preço'),
                'index' => 'param_id',  // this is optional parameter (if you want to pass one of the available values in collection)
                'filter' => false,
                'renderer' => 'Eparts\PrecificacaoAvancada\Block\Adminhtml\Renderer'
            ]
        );
        return parent::_prepareColumns();
    }

    public function getHeadersVisibility()
    {
        return $this->getCollection()->getSize() >= 0;
    }
}
