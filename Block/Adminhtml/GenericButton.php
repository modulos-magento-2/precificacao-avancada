<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 * @package Eparts\PrecificacaoAvancada\Block\Adminhtml\Companies\Edit
 */
class GenericButton
{
    /**
     * @var mixed
     */
    protected $_urlBuilder;

    /**
     * @var Context
     */
    private $_context;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->_context = $context;
        $this->_urlBuilder = $context->getUrlBuilder();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->_context->getRequest()->getParam('id');
    }

    /**
     * @param string $route
     * @param array $params
     *
     * @return string
     */
    public function getUrl($route = '', $params = []): string
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }
}
