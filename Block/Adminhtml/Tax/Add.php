<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Tax;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Backend\Block\Widget\Form\Container;

class Add extends Container
{
    /**
     * @var Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * Add constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_objectId = 'row_id';
        $this->_blockGroup = 'Eparts_precificacaoAvancada';
        $this->_controller = 'adminhtml_tax';
        $this->buttonList->remove('reset');
        $request = $this->getRequest()->getParams();
        if (isset($request['id']) && !empty($request['id'])) {
            $this->addButton(
                'delete',
                [
                    'label' => __('Delete'),
                    'onclick' => 'deleteConfirm(' . json_encode(__('Deseja apagar esse registro?'))
                        . ','
                        . json_encode($this->getDeleteUrl())
                        . ')',
                    'class' => 'scalable delete',
                    'level' => -1
                ]
            );
        }
    }

    /**
     * @return string
     */
    public function getDeleteUrl(): string
    {
        $request = $this->getRequest()->getParams();
        $id = $request['id'];
        return $this->getUrl('*/*/delete', ['id' => $id]);
    }

    /**
     * @return Phrase
     */
    public function getHeaderText(): Phrase
    {
        return __('Cadastrar Imposto');
    }

    /**
     * @return array|mixed|string|null
     */
    public function getFormActionUrl()
    {
        if ($this->hasFormActionUrl()) {
            return $this->getData('form_action_url');
        }

        return $this->getUrl('*/*/save');
    }
}
