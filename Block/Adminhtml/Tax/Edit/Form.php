<?php

namespace Eparts\PrecificacaoAvancada\Block\Adminhtml\Tax\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;


    /**
     * @param \Magento\Backend\Block\Template\Context $context,
     * @param \Magento\Framework\Registry $registry,
     * @param \Magento\Framework\Data\FormFactory $formFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }


    /**
     * @return \Magento\Backend\Block\Widget\Form\Generic
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('row_data');
        $form = $this->_formFactory->create(['data' => [
                'id' => 'edit_form',
                'enctype' => 'multipart/form-data',
                'action' => $this->getData('action'),
                'method' => 'post'
                ]
            ]
        );

        $form->setHtmlIdPrefix('eparts_tax_');
        $type = '';
        if ($model->getEntityId()) {
            $type = $model->getTypeDiscount();
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Editar Imposto'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Cadastrar Imposto'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField('name', 'text', [
                'name' => 'name',
                'label' => __('Nome da Regra'),
                'id' => 'name',
                'title' => __('Nome da Regra'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );


        $fieldset->addField('type', 'select', [
                'name' => 'type',
                'value' => $type,
                'label' => __('Tipo de Desconto'),
                'id' => 'type',
                'title' => __('Tipo de Desonto'),
                'values' => ['percent' => 'Percentual %', 'fixed' => 'Valor Fixo R$'],
                'class' => 'status',
                'required' => true,
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
