<?php

namespace Eparts\PrecificacaoAvancada\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;
use Eparts\PrecificacaoAvancada\Model\Pricesperregions;


class Modal extends Template
{

    /**
     * @var Session
     */
    protected $sessionCustomer;

    /**
     * @var RegionsFactory
     */
    protected $regionFactory;

    /**
     * @var Pricesperregions
     */
    protected $pricePerRegion;

    /**
     * @var array
     */
    protected $regions = [];

    /**
     * Modal constructor.
     * @param Session $sessionCustomer
     * @param RegionsFactory $regionFactory
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Session $sessionCustomer,
        RegionsFactory $regionFactory,
        Pricesperregions $pricesperregions,
        Context $context,
        array $data = []
    )
    {
        $this->sessionCustomer = $sessionCustomer;
        $this->regionFactory = $regionFactory;
        $this->pricePerRegion = $pricesperregions;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function showPopup(): bool
    {
        $this->regions = $this->getRegions();
        if (!$this->sessionCustomer->isLoggedIn() &&
            $this->regions &&
            !$this->sessionCustomer->getRegionSelected()
        ) {
            $priceDefault = $this->getPriceDefault();
            if ($priceDefault && !$this->sessionCustomer->getRegionSelected()) {
                $this->sessionCustomer->setCustomerGroupId($priceDefault);
            }

            return true;
        }



        return false;
    }

    /**
     * @return array|false
     */
    public function getRegions()
    {
        if (!empty($this->regions)) {
            return $this->regions;
        }

        $regions = $this->regionFactory->create()
            ->getCollection()
            ->addFieldToFilter('status', 1);

        if (!$regions->count()) {
            return false;
        }

        $data = [];
        foreach ($regions as $region) {
            $data[] = [
                'value' => $region->getId(),
                'label' => $region->getName()
            ];
        }

        return $data;
    }


    /**
     * @return false|string
     * @throws NoSuchEntityException
     */
    public function getPriceDefault()
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();

        $collection = $this->pricePerRegion
            ->getCollection()
            ->addFieldToFilter('region_id', 0);

        if (!$collection->count()) {
            return false;
        }

        foreach ($collection as $prices) {
            if ($websiteId == $prices->getWebsiteId() || empty($prices->getWebsiteId())) {
                return $prices->getCustomerGroupId();
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getFormUrl(): string
    {
        return $this->getUrl('prices/regions/prices');

    }

    /**
     * @return mixed
     */
    public function showHeader()
    {
        if ($this->sessionCustomer->isLoggedIn()) {
            return false;
        }

        return $this->sessionCustomer->getRegionSelected();
    }

    public function getRegionName()
    {
        $id = $this->sessionCustomer->getRegionSelected();
        $region = $this->regionFactory->create()->load($id);
        return $region->getName();
    }
}
