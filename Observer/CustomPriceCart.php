<?php

namespace Eparts\PrecificacaoAvancada\Observer;

use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Session;
use Eparts\PrecificacaoAvancada\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;


class CustomPriceCart implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * CustomPriceCart constructor.
     * @param Session $session
     * @param Data $data
     */
    public function __construct(Session $session, Data $data)
    {
        $this->session = $session;
        $this->helper = $data;
    }


    /**
     * @param Observer $observer
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $items = $this->session->getQuote()->getAllItems();
        if ($items) {
            $this->helper->updatePrices();
        }
    }
}
