<?php

namespace Eparts\PrecificacaoAvancada\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Eparts\PrecificacaoAvancada\Helper\Data;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class SaveCustomer
 * @package Eparts\PrecificacaoAvancada\Observer
 */
class SaveCustomer implements ObserverInterface
{
    protected $customerSession;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * SaveCustomer constructor.
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param Data $helper
     */
    public function __construct(
        Session $customerSession,
        CheckoutSession $checkoutSession,
        Data $helper
    )
    {
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }

    /**
     * @param EventObserver $observer
     * @return SaveCustomer
     * @throws NoSuchEntityException
     */
    public function execute(EventObserver $observer): SaveCustomer
    {
        $customer = $observer->getCustomer();

        $regionIdSession = $this->customerSession->getRegionSelected();
        if (!$regionIdSession) {
            return $this;
        }


        $groupId = $this->helper->getCustomerByRegion($regionIdSession);
        if ($groupId && $groupId != $customer->getGroupId()) {
            $customer->setGroupId($groupId)
                ->save();
        }

        return $this;
    }
}
