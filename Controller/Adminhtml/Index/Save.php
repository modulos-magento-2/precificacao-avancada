<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Index;

use Eparts\PrecificacaoAvancada\Model\PricesFactory;
use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    protected $pricesFacoty;

    public function __construct(
        Action\Context $context,
        PricesFactory $pricesFacoty
    )
    {
        $this->pricesFacoty = $pricesFacoty;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        try {
            $model = $this->pricesFacoty->create();
            $model->setData($data);
            $model->save();
            $this->messageManager->addSuccess(__('Preço salvo com sucesso!'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Erro ao salvar preço '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/add', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('customer/*/edit' , array('id' => $data['customer_id']));
    }
}
