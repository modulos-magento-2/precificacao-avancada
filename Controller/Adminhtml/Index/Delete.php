<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Index;

use Eparts\PrecificacaoAvancada\Model\Prices;
use Magento\Backend\App\Action;

class Delete extends Action
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $priceCustomer = $this->_objectManager->create(Prices::class)->load($id);
        try {
            $priceCustomer->delete();
            $this->messageManager->addSuccess(__('Preço deletado com sucesso'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Erro ao deletar Preço '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }
}
