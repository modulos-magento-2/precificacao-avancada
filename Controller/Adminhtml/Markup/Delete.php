<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Markup;

use Eparts\PrecificacaoAvancada\Model\MarkupFactory;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Markup
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var MarkupFactory
     */
    protected $gridFactory;

    /**
     * @param Context $context
     * @param MarkupFactory $gridFactory
     */
    public function __construct(
        Context $context,
        MarkupFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }


    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (!$data) {
            $this->_redirect('*/*/index');
            return;
        }

        try {
            $model = $this->gridFactory->create()->load($data['id']);
            $model->delete();
            $this->messageManager->addSuccess(__('Markup deletado com sucesso.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
