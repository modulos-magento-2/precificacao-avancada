<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Pricesperregions;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Eparts\PrecificacaoAvancada\Model\PricesperregionsFactory;

/**
 * Class Save
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Pricesperregion
 */
class Save extends Action
{
    /**
     * @var PricesperregionsFactory
     */
    var $gridFactory;

    /**
     * @param Context $context
     * @param RegionsFactory $gridFactory
     */
    public function __construct(
        Context $context,
        PricesperregionsFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('*/pricesperregions/addrow');
            return;
        }
        try {
            $rowData = $this->gridFactory->create();
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setCompanyId($data['id']);
            }

            $rowData->save();
            $this->messageManager->addSuccess(__('price saves successfully'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/pricesperregions/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed(): bool
    {
        return true;
    }
}
