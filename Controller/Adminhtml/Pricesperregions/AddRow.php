<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Pricesperregions;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Eparts\PrecificacaoAvancada\Model\PricesperregionsFactory;

/**
 * Class AddRow
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Pricesperregion
 */
class AddRow extends Action
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var PricesperregionsFactory
     */
    private $gridFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry ,
     * @param PricesperregionsFactory $gridFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PricesperregionsFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->gridFactory = $gridFactory;
    }

    /**
     * Mapped Grid List page.
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $rowId = (int)$this->getRequest()->getParam('id');
        $rowData = $this->gridFactory->create();
        /** @var Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            if (!$rowData->getId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('*/regions/*');
                return $resultPage;
            }
        }

        $this->coreRegistry->register('prices_per_regions', $rowData);
        $title = __('Preço por Região');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed(): bool
    {
        return true;
    }
}
