<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


/**
 * Class Index
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions
 */
class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Eparts_PrecificacaoAvancada::precificacao_avancada');
        $resultPage->addBreadcrumb(__('Regiões'), __('Regiões'));
        $resultPage->getConfig()->getTitle()->prepend((__('Regiões')));

        return $resultPage;
    }

    /***
     * @return bool
     */
    protected function _isAllowed(): bool
    {
       return true;
    }

}
