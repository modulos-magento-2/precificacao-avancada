<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;

/**
 * Class Save
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions
 */
class Save extends Action
{
    /**
     * @var RegionsFactory
     */
    var $gridFactory;

    /**
     * @param Context $context
     * @param RegionsFactory $gridFactory
     */
    public function __construct(
        Context $context,
        RegionsFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('*/regions/addrow');
            return;
        }
        try {
            $rowData = $this->gridFactory->create();
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setCompanyId($data['id']);
            }

            $rowData->save();
            $this->messageManager->addSuccess(__('Regions saves successfully'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/regions/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed(): bool
    {
        return true;
    }
}
