<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;

/**
 * Class AddRow
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Regions
 */
class AddRow extends Action
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var RegionsFactory
     */
    private $gridFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry ,
     * @param RegionsFactory $gridFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        RegionsFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->gridFactory = $gridFactory;
    }

    /**
     * Mapped Grid List page.
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $rowId = (int)$this->getRequest()->getParam('id');
        $rowData = $this->gridFactory->create();
        /** @var Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $rowTitle = $rowData->getName();
            if (!$rowData->getId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('*/regions/*');
                return $resultPage;
            }
        }

        $this->coreRegistry->register('prices_regions', $rowData);
        $title = $rowId ? __('Edit Region') . $rowTitle : __('Regiões');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed(): bool
    {
        return true;
    }
}
