<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax;

use Eparts\PrecificacaoAvancada\Model\TaxFactory;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var TaxFactory
     */
    protected $gridFactory;

    /**
     * @param Context $context
     * @param TaxFactory $gridFactory
     */
    public function __construct(
        Context $context,
        TaxFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }


    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (!$data) {
            $this->_redirect('*/*/index');
            return;
        }

        try {
            $model = $this->gridFactory->create()->load($data['id']);
            if ($model->getColumnImposto() == 'imposto_1') {
                $this->messageManager->addError(__('O imposto 1 não pode ser apagado'));
                return $this->_redirect('*/*/index');
            }
            $model->delete();
            $this->messageManager->addSuccess(__('Imposto apagado com sucesso.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
