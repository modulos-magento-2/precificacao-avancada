<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax;

use Eparts\PrecificacaoAvancada\Model\TaxFactory;
use Magento\Backend\App\Action\Context;

/**
 * Class Save
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var TaxFactory
     */
    protected $gridFactory;

    /**
     * @param Context $context
     * @param TaxFactory $gridFactory
     */
    public function __construct(
        Context $context,
        TaxFactory $gridFactory
    )
    {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }


    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('*/*/add');
            return;
        }

        try {
            $collection = $this->gridFactory->create()->getCollection();
            if (!$collection->count() && !isset($data['entity_id'])) {
                $baseCalculate = 'VALOR COM IMPOSTOS POR DENTRO';
            } else {
                $baseCalculate = 'SUB-TOTAL COM TAXA DE IMPOSTO 1 POR FORA';
            }

            if (!isset($data['entity_id'])) {
                $idImposto = $collection->count() + 1;
                $data['column_imposto'] = 'imposto_' . $idImposto;
                $data['base_calculate'] = $baseCalculate;
            }

            $rowData = $this->gridFactory->create();
            $rowData->setData($data);

            if (isset($data['entity_id'])) {
                $rowData->setEntityId($data['entity_id']);
            }

            $rowData->save();
            $this->messageManager->addSuccess(__('Imposto salvo com sucesso.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
