<?php

namespace Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax;

use Eparts\PrecificacaoAvancada\Model\TaxFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

/**
 * Class Add
 * @package Eparts\PrecificacaoAvancada\Controller\Adminhtml\Tax
 */
class Add extends \Magento\Backend\App\Action
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var TaxFactory
     */
    private $gridFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry,
     * @param TaxFactory $gridFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        TaxFactory $gridFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->gridFactory = $gridFactory;
    }

    /**
     * Mapped Grid List page.
     * @return Page
     */
    public function execute()
    {
        $rowId = (int) $this->getRequest()->getParam('id');
        $rowData = $this->gridFactory->create();

        /** @var Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $rowTitle = $rowData->getTitle();
        }

        $this->coreRegistry->register('row_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Editar Imposto ').$rowTitle : __('Cadastrar Imposto');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return true;
    }
}
