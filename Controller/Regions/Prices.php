<?php

namespace Eparts\PrecificacaoAvancada\Controller\Regions;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultInterface;
use Magento\Customer\Model\Session;
use Eparts\PrecificacaoAvancada\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session as CheckoutSession;

class Prices extends Action
{
    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * Prices constructor.
     * @param ResultFactory $resultFactory
     * @param Session $customerSession
     * @param Data $helper
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        ResultFactory $resultFactory,
        Session $customerSession,
        Data $helper,
        Context $context,
        CheckoutSession $checkoutSession
    )
    {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
        $this->customerSession = $customerSession;
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;

    }

    /**
     * @return ResultInterface
     * @throws NoSuchEntityException|LocalizedException
     */
    public function execute(): ResultInterface
    {
        $request = $this->_request->getParams();
        $quote = $this->checkoutSession->getQuote();
        if (isset($request['regions'])) {
            $groupId = $this->helper->getCustomerByRegion($request['regions']);
            $this->customerSession->setCustomerGroupId($groupId);
            if (!empty($quote)) {
                $quote->setCustomerGroupId($groupId)->save();
            }
            $this->customerSession->setRegionSelected($request['regions']);
            $this->messageManager->addSuccessMessage('Região selecionada com sucesso');
            $this->helper->updatePrices();
        }

        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/');

        return $redirect;
    }

}
