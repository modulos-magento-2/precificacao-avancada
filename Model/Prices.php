<?php

namespace Eparts\PrecificacaoAvancada\Model;
class Prices extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'eparts_precificacao_avancada';

    protected $_cacheTag = 'eparts_precificacao_avancada';

    protected $_eventPrefix = 'eparts_precificacao_avancada';

    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
