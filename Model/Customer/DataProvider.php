<?php

namespace Eparts\PrecificacaoAvancada\Model\Customer;

use Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices\Collection;
use Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $loadedData;
    protected $collectionFactory;
    protected $name;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Collection $collection,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    )
    {
        $this->name = $name;
        $this->collection = $collection;
        $this->collectionFactory = $collectionFactory;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $collection = $this->rowCollection->create()->setOrder('position', 'ASC');
        $items      = $collection->getItems();
        foreach ($items as $item) {
            $this->loadedData['stores']['dynamic_rows_container'][] = $item->getData();
        }

        return $this->loadedData;
    }
}
