<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel;


class Prices extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('eparts_precificacao_avancada', 'id');
    }

}
