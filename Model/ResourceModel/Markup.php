<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel;


class Markup extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('eparts_prices_markup', 'entity_id');
    }

}
