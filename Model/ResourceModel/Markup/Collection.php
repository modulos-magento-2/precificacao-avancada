<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel\Markup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'eparts_precificacao_avancada_markup_collection';
    protected $_eventObject = 'precificacao_avancada_tax_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\Markup', 'Eparts\PrecificacaoAvancada\Model\ResourceModel\Markup');
    }

}

