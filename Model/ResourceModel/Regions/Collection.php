<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel\Regions;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Eparts\PrecificacaoAvancada\Model\ResourceModel\Regions
 */
class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Eparts\PrecificacaoAvancada\Model\Regions::class,
            \Eparts\PrecificacaoAvancada\Model\ResourceModel\Regions::class
        );
    }
}
