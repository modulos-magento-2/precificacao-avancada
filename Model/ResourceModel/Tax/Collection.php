<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel\Tax;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'eparts_precificacao_avancada_tex_collection';
    protected $_eventObject = 'precificacao_avancada_tax_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\Tax', 'Eparts\PrecificacaoAvancada\Model\ResourceModel\Tax');
    }

}

