<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Regions
 * @package Eparts\PrecificacaoAvancada\Model\ResourceModel
 */
class Pricesperregions extends AbstractDb
{

    const ID_FIELD = 'id';

    const TABLE_NAME = 'eparts_prices_per_regions';

    /**
     * Companies constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::ID_FIELD);
    }

}

