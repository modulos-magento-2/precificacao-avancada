<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel;


class Tax extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('eparts_types_tax', 'entity_id');
    }

}
