<?php

namespace Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'eparts_precificacao_avancada_collection';
    protected $_eventObject = 'precificacao_avancada_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\Prices', 'Eparts\PrecificacaoAvancada\Model\ResourceModel\Prices');
    }

}

