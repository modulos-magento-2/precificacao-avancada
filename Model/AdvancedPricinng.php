<?php

namespace Eparts\PrecificacaoAvancada\Model;

use Magento\AdvancedPricingImportExport\Model\Import\AdvancedPricing;
use Magento\CatalogImportExport\Model\Import\Product as ImportProduct;
use Magento\CatalogImportExport\Model\Import\Product\RowValidatorInterface as ValidatorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Eparts\PrecificacaoAvancada\Model\TaxFactory;
use Magento\Catalog\Model\ProductRepository;
use Eparts\PrecificacaoAvancada\Model\MarkupFactory;

use Magento\Framework\App\PageCache\Version;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

class AdvancedPricinng extends AdvancedPricing
{
    const COL_LINHA_PRODUTO = 'linha_de_produto';
    const COL_RULE_TYPE = 'rule_type';
    const COL_TAX_INSIDE = 'tax_inside';
    const COL_NCM = 'ncm';


    protected $cacheTypeList;
    protected $cacheFrontendPool;

    protected $validColumnNames = [
        self::COL_SKU,
        self::COL_LINHA_PRODUTO,
        self::COL_TIER_PRICE_WEBSITE,
        self::COL_TIER_PRICE_CUSTOMER_GROUP,
        self::COL_TIER_PRICE_QTY,
        self::COL_TIER_PRICE,
        self::COL_TIER_PRICE_TYPE,
        self::COL_RULE_TYPE,
        self::COL_TAX_INSIDE,
        self::COL_NCM,
    ];

    /**
     * @var \Eparts\PrecificacaoAvancada\Model\TaxFactory
     */
    protected $taxFactory;

    /**
     * @var array
     */
    protected $taxOutside = [];

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var MarkupFactory
     */
    protected $markupFactory;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Eav\Model\Config $config,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory $resourceFactory,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Helper\Data $catalogData,
        ImportProduct\StoreResolver $storeResolver,
        ImportProduct $importProduct,
        AdvancedPricing\Validator $validator,
        AdvancedPricing\Validator\Website $websiteValidator,
        AdvancedPricing\Validator\TierPrice $tierPriceValidator,
        TaxFactory $taxFactory,
        ProductRepository $productRepository,
        MarkupFactory $markupFactory,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    )
    {
        $this->taxFactory = $taxFactory;
        $this->productRepository = $productRepository;
        $this->markupFactory = $markupFactory;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;

        foreach ($this->getTaxValues() as $tax) {
            $this->taxOutside[] = [
                'type' => $tax['type'],
                'value' => strtolower($tax['value']),
                'name' => $tax['name']
            ];
            array_push($this->validColumnNames, strtolower($tax['value']));
        }

        parent::__construct(
            $jsonHelper,
            $importExportData,
            $importData,
            $config,
            $resource,
            $resourceHelper,
            $string,
            $errorAggregator,
            $dateTime,
            $resourceFactory,
            $productModel,
            $catalogData,
            $storeResolver,
            $importProduct,
            $validator,
            $websiteValidator,
            $tierPriceValidator
        );
    }

    /**
     * Save and replace advanced prices
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @throws \Exception
     */
    protected function saveAndReplaceAdvancedPrices()
    {
        $behavior = $this->getBehavior();
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
            $this->_cachedSkuToDelete = null;
        }

        $listSku = [];
        $tierPrices = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {

                if (isset($rowData[self::COL_RULE_TYPE]) && strtoupper($rowData[self::COL_RULE_TYPE]) == 'MARK UP') {
                    $priceCost = $this->getPriceCost($rowData[self::COL_SKU]);
                    if (empty($priceCost)) {
                        continue;
                    }
                    $totalApply = $rowData[self::COL_TAX_INSIDE] + $rowData[self::COL_TIER_PRICE];

                    $valueWithTaxInside = $priceCost / (100 - $totalApply) * 100;
                    $valueWithTaxInside = (float)number_format($valueWithTaxInside, 2, '.', '');
                    $priceNet = $priceCost /(100-$rowData['tier_price']) * 100;
                    $priceNet = (float)number_format($priceNet, 2, '.', '');
                    $countTax = 1;
                    $finalValue = $valueWithTaxInside;

                    // Soma impostos por fora
                    $arrTax = [];
                    foreach ($this->taxOutside as $taxOutSideValue) {
                        $arrTax[] = [
                            'name' => $taxOutSideValue['name'],
                            'value' => $rowData[$taxOutSideValue['value']],
                            'type' => $taxOutSideValue['type']
                        ];

                        $columnData = $taxOutSideValue['value'];
                        if ($countTax == 1) {
                            if ($taxOutSideValue['type'] == 'percent') {
                                $decimalTax = $rowData[$columnData] / 100;
                                $valueTax = $valueWithTaxInside * $decimalTax;
                                $finalValue = $valueWithTaxInside + $valueTax;
                            } else {
                                $finalValue = $valueWithTaxInside + $rowNum[$columnData];
                            }
                        } else {
                            if ($taxOutSideValue == 'percent') {
                                $decimalTax = $rowData[$columnData] / 100;
                                $valueTax = $finalValue * $decimalTax;
                                $finalValue = $finalValue + $valueTax;
                            } else {
                                $finalValue += $rowData[$columnData];
                            }
                        }
                        $countTax++;
                    }

                    // Salvar valor na tabela
                    $customerGroupId = $this->getCustomerGroupId($rowData[self::COL_TIER_PRICE_CUSTOMER_GROUP]);
                    $websiteIds = explode(',', $rowData[self::COL_TIER_PRICE_WEBSITE]);
                    foreach ($websiteIds as $website) {
                        $data = [
                            'price' => $finalValue,
                            'price_cost' => $priceCost,
                            'price_net' => $priceNet,
                            'customer_group_id' => $customerGroupId,
                            'website_id' => $this->getWebSiteId($website),
                            'sku' => $rowData[self::COL_SKU],
                            'tax_json' => json_encode($arrTax),
                            'margin_markup' => $rowData[self::COL_TIER_PRICE],
                            'tax_inside' => $rowData[self::COL_TAX_INSIDE],
                            'ncm' => $rowData[self::COL_NCM]
                        ];

                        $collectionMarkup = $this->markupFactory->create()
                            ->getCollection()
                            ->addFieldToFilter('sku', $rowData[self::COL_SKU])
                            ->addFieldToFilter('website_id', $data['website_id'])
                            ->addFieldToFilter('customer_group_id', $data['customer_group_id'])
                            ->getFirstItem();


                        $modelMarkupSave = $this->markupFactory->create();
                        if ($collectionMarkup->getEntityId()) {
                            $modelMarkupSave->load($collectionMarkup->getEntityId());
                            $data['entity_id'] = $collectionMarkup->getEntityId();
                            $this->countItemsUpdated++;

                        }

                        $modelMarkupSave->setData($data)
                            ->save();

                        $this->countItemsCreated++;
                    }


                } else {
                    if (!$this->validateRow($rowData, $rowNum)) {
                        $this->addRowError(ValidatorInterface::ERROR_SKU_IS_EMPTY, $rowNum);
                        continue;
                    }
                    if ($this->getErrorAggregator()->hasToBeTerminated()) {
                        $this->getErrorAggregator()->addRowToSkip($rowNum);
                        continue;
                    }

                    $rowSku = $rowData[self::COL_SKU];
                    $listSku[] = $rowSku;

                    $arraySku = [];
                    if (!empty($rowData[self::COL_LINHA_PRODUTO])) {
                        $arraySku = $this->loadProductByLinha($rowData[self::COL_LINHA_PRODUTO]);
                    }

                    # Se tiver a coluna linha de produto preenchido
                    if (!empty($arraySku)) {
                        foreach ($arraySku as $sku) {
                            if (!empty($rowData[self::COL_TIER_PRICE_WEBSITE])) {
                                $tierPrices[$sku][] = [
                                    'all_groups' => $rowData[self::COL_TIER_PRICE_CUSTOMER_GROUP] == self::VALUE_ALL_GROUPS,
                                    'customer_group_id' => $this->getCustomerGroupId(
                                        $rowData[self::COL_TIER_PRICE_CUSTOMER_GROUP]
                                    ),
                                    'qty' => $rowData[self::COL_TIER_PRICE_QTY],
                                    'value' => $rowData[self::COL_TIER_PRICE_TYPE] === self::TIER_PRICE_TYPE_FIXED
                                        ? $rowData[self::COL_TIER_PRICE] : 0,
                                    'percentage_value' => $rowData[self::COL_TIER_PRICE_TYPE] === self::TIER_PRICE_TYPE_PERCENT
                                        ? $rowData[self::COL_TIER_PRICE] : null,
                                    'website_id' => $this->getWebSiteId($rowData[self::COL_TIER_PRICE_WEBSITE])
                                ];
                            }
                        }
                    } else {
                        if (!empty($rowData[self::COL_TIER_PRICE_WEBSITE])) {
                            $websiteIds = explode(',', $rowData[self::COL_TIER_PRICE_WEBSITE]);

                            foreach ($websiteIds as $website) {
                                $tierPrices[$rowSku][] = [
                                    'all_groups' => $rowData[self::COL_TIER_PRICE_CUSTOMER_GROUP] == self::VALUE_ALL_GROUPS,
                                    'customer_group_id' => $this->getCustomerGroupId(
                                        $rowData[self::COL_TIER_PRICE_CUSTOMER_GROUP]
                                    ),
                                    'qty' => $rowData[self::COL_TIER_PRICE_QTY],
                                    'value' => $rowData[self::COL_TIER_PRICE_TYPE] === self::TIER_PRICE_TYPE_FIXED
                                        ? $rowData[self::COL_TIER_PRICE] : 0,
                                    'percentage_value' => $rowData[self::COL_TIER_PRICE_TYPE] === self::TIER_PRICE_TYPE_PERCENT
                                        ? $rowData[self::COL_TIER_PRICE] : null,
                                    'website_id' => $this->getWebSiteId($website)
                                ];
                            }
                        }
                    }
                }
            }

            if (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->processCountExistingPrices($tierPrices, self::TABLE_TIER_PRICE)
                    ->processCountNewPrices($tierPrices);

                $this->saveProductPrices($tierPrices, self::TABLE_TIER_PRICE);
                if ($listSku) {
                    $this->setUpdatedAt($listSku);
                }
            }
        }

        if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
            if ($listSku) {
                $this->processCountNewPrices($tierPrices);
                if ($this->deleteProductTierPrices(array_unique($listSku), self::TABLE_TIER_PRICE)) {
                    $this->saveProductPrices($tierPrices, self::TABLE_TIER_PRICE);
                    $this->setUpdatedAt($listSku);
                }
            }
        }

        $this->flushCache();

        return $this;
    }

    public function validateRow(array $rowData, $rowNum)
    {
        $sku = false;
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }
        $this->_validatedRows[$rowNum] = true;

        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            if (!isset($rowData[self::COL_SKU]) && !isset($rowData[self::COL_LINHA_PRODUTO])) {
                $this->addRowError(ValidatorInterface::ERROR_SKU_IS_EMPTY, $rowNum);
                return false;
            }
            return true;
        }

        if (!$this->_getValidator(self::VALIDATOR_MAIN)->isValid($rowData)) {
            foreach ($this->_getValidator(self::VALIDATOR_MAIN)->getMessages() as $message) {
                $this->addRowError($message, $rowNum);
            }
        }

        if (isset($rowData[self::COL_SKU])) {
            $sku = $rowData[self::COL_SKU];
        }

        if (false === $sku && empty($rowData[self::COL_LINHA_PRODUTO])) {
            $this->addRowError(ValidatorInterface::ERROR_ROW_IS_ORPHAN, $rowNum);
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * @param $attribute
     * @return array
     */
    protected function loadProductByLinha($attribute)
    {
        $collection = $this->_productModel->getCollection()->addAttributeToFilter(self::COL_LINHA_PRODUTO, $attribute);

        $skus = [];
        foreach ($collection as $object) {
            $skus[] = $object->getSku();
        }

        return $skus;
    }

    /**
     * @param $sku
     * @return float
     * @throws NoSuchEntityException
     */
    protected function getPriceCost($sku): float
    {
        $product = $this->productRepository->get($sku);
        if ($product->getCost()) {
            return floatval($product->getCost());
        }

        return 0.0;
    }

    protected function getWebSiteId($websiteCode)
    {
        $result = $websiteCode == $this->_getValidator(self::VALIDATOR_WEBSITE)->getAllWebsitesValue() ||
        $this->_catalogData->isPriceGlobal() ? 0 : $this->_storeResolver->getWebsiteCodeToId($websiteCode);
        return $result;
    }

    /**
     * @return array
     */
    protected function getTaxValues(): array
    {
        $colletion = $this->taxFactory->create()->getCollection();
        $arr = [];
        foreach ($colletion as $data) {
            $arr[] = [
                'value' => $data->getColumnImposto(),
                'type' => $data->getType(),
                'name' => $data->getName()
            ];
        }

        return $arr;
    }

    /**
     * clear cache after load prices
     */
    protected function flushCache()
    {
        $_types = [
            'layout',
            'block_html',
            'collections',
            'reflection',
            'db_ddl',
            'full_page',
        ];

        foreach ($_types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }

}
