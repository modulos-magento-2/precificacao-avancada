<?php

namespace Eparts\PrecificacaoAvancada\Model;

use Eparts\PrecificacaoAvancada\Helper\Data as HelperPrice;
use Magento\Customer\Model\Session as CustomerSession;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CartInterfaceFactory;
use Magento\Quote\Api\Data\CartSearchResultsInterfaceFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\ResourceModel\Quote\Collection as QuoteCollection;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Quote
 * @package Eparts\PrecificacaoAvancada\Model
 */
class Quote extends \Magento\Quote\Model\QuoteRepository
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var RegionsFactory
     */
    protected $regionsFactory;

    /**
     * @var HelperPrice
     */
    protected $helperPrice;


    public function __construct(
        QuoteFactory $quoteFactory,
        StoreManagerInterface $storeManager,
        QuoteCollection $quoteCollection,
        CartSearchResultsInterfaceFactory $searchResultsDataFactory,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        CollectionProcessorInterface $collectionProcessor = null,
        QuoteCollectionFactory $quoteCollectionFactory = null,
        CartInterfaceFactory $cartFactory = null,
        CustomerSession $customerSession,
        RegionsFactory $regionsFactory,
        HelperPrice $helperPrice
    )
    {
        parent::__construct($quoteFactory, $storeManager, $quoteCollection, $searchResultsDataFactory, $extensionAttributesJoinProcessor, $collectionProcessor, $quoteCollectionFactory, $cartFactory);
        $this->customerSession = $customerSession;
        $this->regionsFactory = $regionsFactory;
        $this->helperPrice = $helperPrice;

    }

    /**
     * @param CartInterface $quote
     * @throws NoSuchEntityException|LocalizedException
     */
    public function save(CartInterface $quote)
    {
        parent::save($quote);

        if (!$this->customerSession->isLoggedIn()) {
            $regionId = $quote->getShippingAddress()->getRegionId();
            $regionSelect = $this->customerSession->getRegionSelected();
            $customerGroup = $this->customerSession->getCustomerGroupId();
            if ($regionSelect && $regionId) {

                $region = $this->regionsFactory->create()
                    ->load($regionSelect);

                $regionIdSelected = $region->getRegionId();

                $customerGroup = $this->helperPrice->getCustomerGroupCheckout($regionId);
                if ($regionIdSelected != $regionId) {
                    $groupDefault = $this->helperPrice->getPriceDefault();

                    if ($customerGroup) {
                        $this->customerSession->setCustomerGroupId($customerGroup);
                        $quote->setCustomerGroupId($customerGroup)
                            ->save();
                    } else {
                        // Se não tiver região, seta a tabela de preço default
                        $this->customerSession->setCustomerGroupId($groupDefault);
                        $quote->setCustomerGroupId($groupDefault)
                            ->save();
                    }
                } else {
                    $quote->setCustomerGroupId($customerGroup)
                        ->save();
                }
            }

            $this->helperPrice->updatePrices($customerGroup);
        }
    }
}
