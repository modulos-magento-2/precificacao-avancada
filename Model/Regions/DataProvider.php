<?php

namespace Eparts\PrecificacaoAvancada\Model\Regions;

use Eparts\PrecificacaoAvancada\Model\ResourceModel\Regions\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\Request\Http;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $_dataPersistor;

    /**
     * @var Http
     */
    protected $_request;


    /**
     * @var mixed
     */
    protected $_loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     * @param Http $request
     * @param DataPersistorInterface $dataPersistor
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Http $request,
        DataPersistorInterface $dataPersistor,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    )
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->collection = $collectionFactory->create();
        $this->_request = $request;
        $this->_dataPersistor = $dataPersistor;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $id = $this->_request->getParam('id');

        if ($id) {
            $item = $this->collection->addFieldToFilter('id', $id)->getFirstItem();
            $this->_loadedData[$item->getData('id')] = $item->getData();
        }

        return $this->_loadedData;
    }
}
