<?php

namespace Eparts\PrecificacaoAvancada\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Regions
 * @package Eparts\PrecificacaoAvancada\Model
 */
class Pricesperregions extends AbstractModel
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Pricesperregions::class);
    }
}
