<?php

namespace Eparts\PrecificacaoAvancada\Model\Validator;

use Magento\AdvancedPricingImportExport\Model\Import\AdvancedPricing;

/**
 * Class Website
 * @package Eparts\PrecificacaoAvancada\Model\Validator
 */
class Website extends \Magento\AdvancedPricingImportExport\Model\Import\AdvancedPricing\Validator\Website
{

    /**
     * Validate by website type
     *
     * @param array $value
     * @param string $websiteCode
     * @return bool
     */
    protected function isWebsiteValid($value, $websiteCode)
    {
        $websites = explode(',', $value[$websiteCode]);

        foreach ($websites as $website) {
            $website = trim($website);
            if (isset($website) && !empty($website)) {
                if ($website != $this->getAllWebsitesValue()
                    && !$this->storeResolver->getWebsiteCodeToId($website)) {
                    return false;
                }
            }
        }

        return true;
    }
}
