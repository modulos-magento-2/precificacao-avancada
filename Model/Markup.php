<?php

namespace Eparts\PrecificacaoAvancada\Model;

/**
 * Class Markup
 * @package Eparts\PrecificacaoAvancada\Model
 */
class Markup extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'eparts_precificacao_avancada_markup';

    protected $_cacheTag = 'eparts_precificacao_avancada_markup';

    protected $_eventPrefix = 'eparts_precificacao_avancada_markup';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\ResourceModel\Markup');
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
