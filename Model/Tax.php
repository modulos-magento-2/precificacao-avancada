<?php

namespace Eparts\PrecificacaoAvancada\Model;

/**
 * Class Tax
 * @package Eparts\PrecificacaoAvancada\Model
 */
class Tax extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'eparts_precificacao_avancada_tax';

    protected $_cacheTag = 'eparts_precificacao_avancada_tax';

    protected $_eventPrefix = 'eparts_precificacao_avancada_tax';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Eparts\PrecificacaoAvancada\Model\ResourceModel\Tax');
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
