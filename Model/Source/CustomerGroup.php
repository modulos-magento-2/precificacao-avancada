<?php

namespace Eparts\PrecificacaoAvancada\Model\Source;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Option\ArrayInterface;
use Magento\Customer\Model\GroupFactory;
use Magento\Framework\DataObject;

class CustomerGroup extends DataObject implements ArrayInterface
{
    /**
     * @var GroupFactory
     */
    protected $group;

    /**
     * Store constructor.
     * @param GroupFactory $group
     */
    public function __construct(
        GroupFactory $group
    )
    {
        $this->group = $group;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $groups = $this->group->create()->getCollection();
        $groupsList = [];

        foreach ($groups as $group) {
            $groupsList[] = [
                'label' => $group->getCustomerGroupCode(),
                'value' => $group->getId()
            ];
        }

        return $groupsList;
    }
}
