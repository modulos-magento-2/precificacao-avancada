<?php

namespace Eparts\PrecificacaoAvancada\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\DataObject;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;

class Regions extends DataObject implements ArrayInterface
{
    /**
     * @var RegionsFactory
     */
    protected $regionsFactory;

    /**
     * Regions constructor.
     * @param RegionsFactory $regionsFactory
     */
    public function __construct(
        RegionsFactory $regionsFactory
    )
    {
        $this->regionsFactory = $regionsFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $regions = $this->regionsFactory->create()->getCollection()
            ->addFieldToFilter('status', 1);

        $regionList[0] = [
            'label' => 'SEM SELEÇÃO',
            'value' => 0
        ];

        foreach ($regions as $region) {
            $regionList[$region->getData('id')] = [
                'label' => $region->getData('name'),
                'value' => $region->getData('id')
            ];
        }

        return $regionList;
    }
}
