<?php

namespace Eparts\PrecificacaoAvancada\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\StoreRepository;
use Magento\Framework\DataObject;

class Stores extends DataObject implements ArrayInterface
{
    /**
     * @var StoreRepository
     */
    protected $_storeRepository;

    /**
     * Store constructor.
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        StoreRepository $storeRepository
    )
    {
        $this->_storeRepository = $storeRepository;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $stores = $this->_storeRepository->getList();
        $storeList = [];

        foreach ($stores as $store) {
            $storeList[] = [
                'label' => strtolower($store->getName()) == 'admin' ? 'TODOS' : strtoupper($store->getName()),
                'value' => $store->getWebsiteId()
            ];
        }

        return $storeList;
    }
}
