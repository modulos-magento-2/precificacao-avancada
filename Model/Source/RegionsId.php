<?php

namespace Eparts\PrecificacaoAvancada\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\DataObject;
use Magento\Directory\Model\Country;

/**
 * Class RegionsId
 * @package Eparts\PrecificacaoAvancada\Model\Source
 */
class RegionsId extends DataObject implements ArrayInterface
{
    /**
     * @var Country
     */
    protected $country;

    /**
     * Regions constructor.
     * @param Country $country
     */
    public function __construct(
        Country $country
    )
    {
        $this->country = $country;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $regions = $this->country
            ->loadByCode('BR')
            ->getRegions();

        $regionList[0] = [
            'label' => 'Selecione',
            'value' => ''
        ];

        foreach ($regions as $region) {
            $regionList[$region->getData('region_id')] = [
                'label' => $region->getData('code'),
                'value' => $region->getData('region_id')
            ];
        }

        return $regionList;
    }
}
