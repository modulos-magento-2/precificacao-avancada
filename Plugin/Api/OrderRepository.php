<?php

namespace Eparts\PrecificacaoAvancada\Plugin\Api;


use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Eparts\PrecificacaoAvancada\Model\MarkupFactory;

class OrderRepository
{

    /**
     * @var
     */
    protected $markupFactory;


    public function __construct(
        MarkupFactory $markupFactory
    )
    {
        $this->markupFactory = $markupFactory;
    }

    public function afterGet(OrderRepositoryInterface $subject, $result, $id)
    {
        $items = $result->getItems();
        foreach ($items as $item) {
            $qty = (int)$item->getQtyOrdered();
            $extensionAttributes = $item->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
            $extensionAttributes->setPriceNet($this->getPriceNet($result, $item) * $qty);
            $item->setExtensionAttributes($extensionAttributes);
        }

        return $result;


    }

    /**
     * @param $result
     * @param $dataList
     * @return mixed
     */
    protected function getPriceNet($result, $item)
    {
        $markup = $this->markupFactory->create()
            ->getCollection()
            ->addFieldToFilter('sku', $item->getSku());

        if (!$markup->count()) {
            return null;
        }

        foreach ($markup as $value) {
            if ($value->getCustomerGroupId() != $result->getCustomerGroupId()) {
                continue;
            }

            if ($value->getWebsiteId() != $result->getStoreId() && !empty($result->getStoreId())) {
                continue;
            }

            return $value->getPriceNet();
        }

        return null;
    }

}
