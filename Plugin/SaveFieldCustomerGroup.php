<?php

namespace Eparts\PrecificacaoAvancada\Plugin;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\GroupFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\App\ResourceConnection;

class SaveFieldCustomerGroup {

    protected $_filterBuilder;
    protected $_groupFactory;
    protected $_groupRepository;
    protected $_searchCriteriaBuilder;
    protected $_resourceConnection;



    public function __construct(
        FilterBuilder $filterBuilder,
        GroupRepositoryInterface $groupRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        GroupFactory $groupFactory,
        ResourceConnection $resourceConnection
    )
    {
        $this->_filterBuilder = $filterBuilder;
        $this->_groupRepository = $groupRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_groupFactory = $groupFactory;
        $this->_resourceConnection = $resourceConnection;
    }

    /**
     * @param \Magento\Customer\Controller\Adminhtml\Group\Save $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(\Magento\Customer\Controller\Adminhtml\Group\Save $subject, $result)
    {
        $policicaPreco = $subject->getRequest()->getParam('politica_preco');
        $id = $subject->getRequest()->getParam('id');

        if (empty($id)) {
            return $result;
        }

        $query = "UPDATE customer_group SET politica_preco = '".$policicaPreco."' WHERE customer_group_id = $id;";
        $this->_resourceConnection->getConnection()->query($query);

        return $result;
    }
}