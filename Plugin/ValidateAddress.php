<?php

namespace Eparts\PrecificacaoAvancada\Plugin;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Checkout\Model\ShippingInformationManagement;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;
use Eparts\PrecificacaoAvancada\Helper\Data as HelperPrice;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ValidateAddress
 * @package Eparts\PrecificacaoAvancada\Plugin
 */
class ValidateAddress
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var RegionsFactory
     */
    protected $regionFactory;

    protected $helperPrice;

    /**
     * ValidateAddress constructor.
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param RegionsFactory $regionsFactory
     * @param HelperPrice $helperPrice
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        RegionsFactory $regionsFactory,
        HelperPrice $helperPrice
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->regionFactory = $regionsFactory;
        $this->helperPrice = $helperPrice;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    )
    {
        $regionId = $addressInformation->getShippingAddress()->getRegionId();
        $regionSelect = $this->customerSession->getRegionSelected();
        if ($regionSelect && $regionId) {

            $region = $this->regionFactory->create()
                ->load($regionSelect);

            $regionIdSelected = $region->getRegionId();

            $customerGroup = $this->helperPrice->getCustomerGroupCheckout($regionId);
            if ($regionIdSelected != $regionId) {
                $groupDefault = $this->helperPrice->getPriceDefault();

                if ($customerGroup) {
                    $this->customerSession->setCustomerGroupId($customerGroup);
                    $this->checkoutSession->getQuote()
                        ->setCustomerGroupId($customerGroup)
                        ->save();
                } else {
                    // Se não tiver região, seta a tabela de preço default
                    $this->customerSession->setCustomerGroupId($groupDefault);
                    $this->checkoutSession->getQuote()
                        ->setCustomerGroupId($groupDefault)
                        ->save();
                }
            } else {
                $this->checkoutSession->getQuote()
                    ->setCustomerGroupId($customerGroup)
                    ->save();
            }
        }
    }


}
