<?php

namespace Eparts\PrecificacaoAvancada\Plugin;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\App\ResourceConnection;

class FormCustomerGroup {

    protected $groupRepository;
    protected $resourceConnection;

    public function __construct(GroupRepositoryInterface $groupRepository, ResourceConnection $resourceConnection)
    {
        $this->groupRepository = $groupRepository;
        $this->resourceConnection = $resourceConnection;
    }

    public function aftersetForm(\Magento\Customer\Block\Adminhtml\Group\Edit\Form $forma)
    {
        $id = $forma->getRequest()->getParam('id');
        $con = $this->resourceConnection->getConnection();
        $query = "select politica_preco from  customer_group where customer_group_id = '".$id."';";
        $value = $con->fetchOne($query);

        $form = $forma->getForm();
        $fieldset = $form->addFieldset('base1_fieldset', ['legend' => __('Política de Preço')]);

        $fieldset->addField('politica_preco',
            'select',
            [
                'name' => 'politica_preco',
                'label' => __('Política de Preço'),
                'title' => __('Política de Preço'),
                'class' => 'required-entry',
                'value' => $value,
                'required' => true,
                'values' => [\Eparts\PrecificacaoAvancada\Helper\Data::POLITICA_DEFINIDA => 'POLÍTICAS COMERCIAIS PRE DEFINIDAS', \Eparts\PrecificacaoAvancada\Helper\Data::POLITICA_VARIAVEL =>'POLÍTICAS VARIAVEL']
            ]);

        return $form;
    }
}
