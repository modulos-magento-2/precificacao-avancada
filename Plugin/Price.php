<?php

namespace Eparts\PrecificacaoAvancada\Plugin;

use Magento\Catalog\Pricing\Price\FinalPrice;
use Eparts\PrecificacaoAvancada\Helper\Data;
use Eparts\PrecificacaoAvancada\Model\MarkupFactory;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

class Price
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var MarkupFactory
     */
    protected $markup;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Session
     */
    protected $session;

    public function __construct(
        Data $helper,
        MarkupFactory $markupFactory,
        Session $session,
        StoreManagerInterface $storeManager

    )
    {
        $this->helper = $helper;
        $this->markup = $markupFactory;
        $this->session = $session;
        $this->storeManager = $storeManager;
    }

    /**
     * @param FinalPrice $subject
     * @param float $result
     * @return float
     */
    public function afterGetValue(FinalPrice $subject, float $result)
    {
        $id = $subject->getProduct()->getId();
        $sku = $subject->getProduct()->getSku();
        $markup = $this->helper->getPriceMarkup($sku);

        if ($markup) {
            return $markup;
        }

        $finalPrice = $this->helper->getDiscountVariable($id);

        if ($finalPrice) {
            return $finalPrice;
        }

        return $result;
    }
}
