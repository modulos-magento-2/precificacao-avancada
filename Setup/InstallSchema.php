<?php

namespace Eparts\PrecificacaoAvancada\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->addColumn(
            $setup->getTable('customer_group'),
            'politica_preco',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'default' => 'POLÍTICAS COMERCIAIS PRE DEFINIDAS',
                'unsigned' => true,
                'comment' => 'política de preço'
            ]
        );

        $setup->endSetup();
    }

}
