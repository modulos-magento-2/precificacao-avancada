<?php

namespace Eparts\PrecificacaoAvancada\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Customer\Setup\CustomerSetupFactory;


class UpgradeData implements UpgradeDataInterface
{

    private $customerSetupFactory;

    /**
     * Constructor
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'atributo_produto', [
                'type' => 'varchar',
                'label' => 'Atributo do Produto',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 2,
                'system' => false,
                'backend' => ''
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'atributo_produto')
                ->addData(['used_in_forms' => ['adminhtml_customer']
                ]);
            $attribute->save();

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'fator_desconto', [
                'type' => 'varchar',
                'label' => 'Fator de Desconto (%)',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 3,
                'system' => false,
                'backend' => ''
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'fator_desconto')
                ->addData(['used_in_forms' => ['adminhtml_customer']
                ]);

            $attribute->save();

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'store_id_desconto', [
                'type' => 'varchar',
                'label' => 'Store Desconto Variavel',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 4,
                'system' => false,
                'backend' => ''
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'store_id_desconto')
                ->addData(['used_in_forms' => ['adminhtml_customer']
                ]);

            $attribute->save();
        }
    }

}
