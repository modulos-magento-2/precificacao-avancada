<?php
namespace Eparts\PrecificacaoAvancada\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '0.1.2') < 0) {
            if (!$installer->tableExists('eparts_precificacao_avancada')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('eparts_precificacao_avancada')
                )
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'customer_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [],
                        'Customer_id'
                    )
                    ->addColumn(
                        'attribute_name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Attribute Name'
                    )
                    ->addColumn(
                        'attribute_value',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Attribute Value'
                    )
                    ->addColumn(
                        'factor_discount',
                        \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        null,
                        [],
                        'Percentual de desconto'
                    )
                    ->addColumn(
                        'website_ids',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'web site ids'
                    )->setComment('Tabela de precificaçãi avançada por cliente');
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('eparts_precificacao_avancada'),
                    $setup->getIdxName(
                        $installer->getTable('eparts_precificacao_avancada'),
                        ['attribute_value','attribute_name'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['attribute_value','attribute_name'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }

        $installer->endSetup();
    }
}
