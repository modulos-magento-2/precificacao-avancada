define([
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal',
], function (Component, $, modal) {
    'use strict';

    return Component.extend({
        initialize: function (config) {
            this._super();
            this.openPopup = config.openPopup;
            let options = {
                type: 'popup',
                responsive: true,
                innerScroll: false,
                title: false,
                buttons: false
            };

            let modal_overlay_element = $('#modal-regions');
            modal(options, modal_overlay_element);
            modal_overlay_element.css("display", "block");

            if (this.openPopup === '1') {
                this.openModalOverlayModal();
            }

            this.changeRegion();
        },

        openModalOverlayModal: function () {
            let modalContainer = $("#modal-regions");
            modalContainer.modal('openModal');
        },

        changeRegion: function () {
            $('#regions').change(function () {
                $("body").trigger('processStart');
                $('#formRegions').submit();
            })
        }
    });
});
