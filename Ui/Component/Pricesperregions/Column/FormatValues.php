<?php

namespace Eparts\PrecificacaoAvancada\Ui\Component\Pricesperregions\Column;

use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\GroupFactory;
use Eparts\PrecificacaoAvancada\Model\RegionsFactory;

/**
 * Class FormatValues
 * @package Eparts\PrecificacaoAvancada\Ui\Component\Pricesperregions\Column
 */
class FormatValues extends Column
{
    protected $storeManager;

    protected $regionFactory;

    protected $groupFactory;

    /**
     * FormatValues constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param GroupFactory $groupFactory
     * @param StoreManagerInterface $storeManager
     * @param RegionsFactory $regionsFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        GroupFactory $groupFactory,
        StoreManagerInterface $storeManager,
        RegionsFactory $regionsFactory,
        array $components = [],
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->groupFactory = $groupFactory;
        $this->regionFactory = $regionsFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        $dataNew = [];
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $item) {
                if (empty($item['region_id'])) {
                    $regionName = 'SEM SELEÇÃO';
                } else {
                    $region = $this->regionFactory->create()->load($item['region_id']);
                    $regionName = $region->getName();
                }
                                $group = $this->groupFactory->create()->load($item['customer_group_id']);
                $store = $this->storeManager->getWebsite($item['website_id'])->getName();
                $dataNew['data']['items'][] = [
                    'id_field_name' => $item['id_field_name'],
                    'id' => $item['id'],
                    'region_id' => $regionName,
                    'customer_group_id' => $group->getCustomerGroupCode(),
                    'website_id' => $store,
                    'orig_data' => $item['orig_data']
                ];
            }
        }

        if (isset($dataNew['data']['items'])) {
            $dataNew['data']['totalRecords'] = count($dataNew['data']['items']);
            return $dataNew;
        }

        return $dataSource;
    }
}
