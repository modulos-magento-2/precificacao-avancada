<?php

namespace Eparts\PrecificacaoAvancada\Ui\Component\Regions\Column;

use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

/**
 * Class FormatValues
 * @package Eparts\PrecificacaoAvancada\Ui\Component\Regions\Column
 */
class FormatValues extends Column
{

    /**
     * FormatValues constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        $dataNew = [];
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $item) {

                $dataNew['data']['items'][] = [
                    'id_field_name' => $item['id_field_name'],
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'status' => $item['status'] == 1 ? 'Habilitado' : 'Desabilitado'
                ];
            }
        }

        if (isset($dataNew['data']['items'])) {
            $dataNew['data']['totalRecords'] = count($dataNew['data']['items']);
            return $dataNew;
        }

        return $dataSource;
    }
}
