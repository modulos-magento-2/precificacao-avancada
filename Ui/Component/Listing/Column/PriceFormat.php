<?php

namespace Eparts\PrecificacaoAvancada\Ui\Component\Listing\Column;

class PriceFormat extends \Magento\Ui\Component\Listing\Columns\Column
{

    protected $group;

    protected $store;

    public function __construct(
        \Magento\Customer\Model\Group $group,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        $this->group = $group;
        $this->store = $storeManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $dataNew = [];
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $item) {

                if (!is_numeric($item['website_id'])) {
                    continue;
                }

                $store = $this->store->getWebsite($item['website_id'])->getName();
                if ($store == 'Admin') {
                    $store = 'Todos';
                }
                $customerGroup = $this->group->load($item['customer_group_id']);

                $taxValues = $item['tax_json'];
                if ($taxValues) {
                    $json = json_decode($taxValues);
                    $taxValues = '';
                    foreach ($json as $taxValue) {
                        $value = number_format($taxValue->value, 2, ',', '.');
                        if ($taxValue->type == 'percent') {
                            $taxValues .= $taxValue->name . ': ' . $value . '%' . ';';
                        } else {
                            $taxValues .= $taxValue->name . ': R$' . $value . ';';
                        }
                    }
                }
                $dataNew['data']['items'][] = [
                    'id_field_name' => $item['id_field_name'],
                    'entity_id' => $item['entity_id'],
                    'price' => 'R$ ' . number_format($item['price'], 2, ',', '.'),
                    'price_cost' => 'R$ ' . number_format($item['price_cost'], 2, ',', '.'),
                    'price_net' => 'R$ ' . number_format($item['price_net'], 2, ',', '.'),
                    'sku' => $item['sku'],
                    'tax_inside' => $item['tax_inside'],
                    'ncm' => $item['ncm'],
                    'margin_markup' => $item['margin_markup'],
                    'website_id' => $store,
                    'tax_json' => $taxValues,
                    'customer_group_id' => $customerGroup->getCustomerGroupCode(),
                    'orig_data' => $item['orig_data']
                ];
            }
        }

        if (isset($dataNew['data']['items'])) {
            $dataNew['data']['totalRecords'] = count($dataNew['data']['items']);
            return $dataNew;
        }

        return $dataSource;
    }
}
